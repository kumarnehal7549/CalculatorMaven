package kumarnehal7549.Calculator;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
	Calculator c = new Calculator();
    
    @Test
	public void testAddition() {
		assertEquals(10, c.Addition(5,5));
		assertEquals(7, c.Addition(11,-4));
		assertEquals(0, c.Addition(5,-5));
		assertEquals(-6, c.Addition(-10,4));
		assertEquals(7, c.Addition(1,6));
	}
    
    @Test
	public void testSubstract() {
		assertEquals(0, c.Subtract(5,5));
		assertEquals(7, c.Subtract(10,3));
		assertEquals(3, c.Subtract(5,2));
		assertEquals(-3, c.Subtract(4,7));
		assertEquals(-6, c.Subtract(0,6));
	}
    
	@Test
	public void testMultiplication()
	{
		assertEquals(25, c.Multiply(5,5));
		assertEquals(-40, c.Multiply(10,-4));
		assertEquals(-21, c.Multiply(7,-3));
		assertEquals(4, c.Multiply(-1,-4));
		assertEquals(0, c.Multiply(0,6));
		
	}
	@Test
	public void testDivision()
	{
		assertEquals(1, c.Division (5,5));
		assertEquals(5, c.Division(25,5));
		assertEquals(-1, c.Division(-5,5));
		assertEquals(-1, c.Division(1,-1));
		assertEquals(3, c.Division(-9,-3));
    }
}
